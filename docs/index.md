# Fabrication de masques anti projections pour les hopitaux dans les fablabs

Le Fablab ULB (Belgique) a développé et produit des masques anti-projections à destination du CHU Saint-Pierre à Bruxelles.

Nous sommes maintenant contactés par plusieurs hôpitaux de Belgique.

Nous sommes actuellement épaulés par le Fab-C à Charleroi et le fablab YourLab à Andenne, d'autres Fablabs vont se joindre à l'aventure.

Voici un tutoriel pour reproduire ces masques dans les différents fablabs.