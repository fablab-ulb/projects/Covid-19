# I M P O R T A N T   N O T E :

From now (2020-03-25 5PM UTC), the only one git you should look at is :

https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields

Everything is merged and documented in real time at that URL.


# Fablab ULB - Protective Face Shield - Cut version

The structure is made of 3 stripes that can be easily assembled. the structure is meant to be cut with a laser cutter in PETG sheet of 1 mm thickness.

Here is the file to be cut.

![](anti_projection_2D-full_v1.svg)

* Here is an example of the assembled sheet (the version on the picture below is not the final version, it is a smaller one).
* The structure of the final version should be cut using a laser cutter and 1mm PETG sheets.
* At the moment, an elastic band can be used to fix it on your head.
* A4 transparent sheet in landscape mode (not like on the picture) should be clamped in between the 2 stripes. When it bends the sheet cannot move.

![](FablabULB-Protective-Face-Shield-cut-version.jpg)

## Next step
* cut "anti_projection_2D-full_v1.svg" in 1 mm PETG sheet
* assemble it with rubber band and a transparent
* get feedback from the MDs.
* improve
