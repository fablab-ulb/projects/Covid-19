# I M P O R T A N T   N O T E :

From now (2020-03-25 5PM UTC), the only one git you should look at is :

https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields

Everything is merged and documented in real time at that URL.
